Bookmyshow Clone Script is completely in built and has automated modules. Online web based benefits of  reservation process, create more sale leads and track customers, and most of all, it helps in maintaining your repeated customers info. You can analyze reservation trend details and work towards your future needs.

Bookmyshow Clone Script comes with Mobile Application – Both Android and IOS.

PRODUCT DESCRIPTION
UNIQUE FEATURES:

Room/Inventory availability
Real Time Pricing
Real Time Booking/Reservation
Online Cancellation/Refund
Booking Amendments
Travel Advisory Services
Destination Management
White Label Solutions
Affiliate Model
Multiple Payment Options
Easy to manage Property Information
Room Type , Inventory
Room Rate Amenities
Seasonal Rate and Other Charges
COMMON  FEATURES

SEARCH OPTIONS

User can search by entering location, check-in and check-out date along with the number of rooms and as well the number of adults/children whose going to reside.
Map search is also available.
FILTER OPTIONS

User can check by selecting the stars (either 3 star hotels or 5 star hotels…etc)
Check by filtering the pay scale ranging from the least amount to the highest.
Can check by selecting a particular location.
HOTEL  INFO

User can view the complete address of the selected hotel.
Can view images and hotel amenities.
Choose the particular room that you wish (Singe/Double/Luxury/Deluxe…etc).
Access to API Hotels and Offer Hotels.
HOTEL ADMIN PANEL

LOGIN

Log in using desired username and password  (provided by admin)
PROFILE

View the Agent details
Edit the agent details, block and unblock the agent record
Change the login Password
HOTEL MANAGEMENT

Add the trip details
Searched by trip details using by  hotel type, from the city, and status
List the  trip details  using on load Ajax functions
Manage the trip details (Edit, delete, block and unblock, pagination)
View and manage details
View the particular hotel details
structures designed by drag and drop method
Can add/delete hotel details.
Manage bookings and tickets list.
Manage image uploads.
Manage rooms.
Manage user info.
Maintain payment gateway and user wallet.
Manage day’s offers.
Complete end-to-end access.
HOTEL IMAGES AND VIDEOS

Add the hotel images
Manage the hotel images(edit, status,delete)
Upload the hotel videos
PASSENGER MANAGEMENT

List the booked traveler details
View the particular guest details
View the booking details
View the particular count and view the hotel details
Searched by hotel details using ticket no , booked date, user type, from city , to city
SEAT MANAGEMENT

Search and then list the seat details
Searched by seat details using date
TRAVELER MANAGEMENT

List the all details
View the particular traveler details
Filtered by traveler details using date,from to city
CANCEL

List all traveler
View the particular details
Filtered by tickets using ticket no, cancel date, travel date
PAYMENT MANAGEMENT

List the hotel details
View the particular payment transaction details
Filtered by trip details using hotel type, from city, to city , date
CANCELLATION POLICIES

Add the refund status
Manage the details(edit,status,delete)
SMS LOG DETAILS

List the sms details
Manage the details(delete)
Search sms details by bus and day,month,year
EMAIL LOG DETAILS

List the email details
Manage the details(delete)
Search details details by bus and day,month,year
MANAGE BANNERS

Banners to pop-up with offers would be generated.
MANAGE MARQUEE TEXT

Texts as flash notes either at the top or bottom of the page could be generated.
COUPON

Day-to-day offer coupon codes would be generated.
HOTEL AGENT PANEL

LOGIN

Login using username and password as registered
CONTROL PANEL

Agent Profile
Deposit
Target and Incentives would be maintained
HOTEL MANAGEMENT

Agent can add the hotel details.
He can view and block the room that’s not of interest.
Manage hotel details (edit/delete/block/unblock).
ROOM MANAGEMENT

View the particular room count and details
View booker and travel details
BOOKING REPORT

Ledger details
Detailed report
Monthly-yearly-today chart would be maintained
REPORTS

Transaction reports would be maintained
WALLET DEPOSIT/PRINT TICKET/CANCEL

Amount in Wallet deposited could be reviewed
Can print or cancel ticket.
HOTEL USER PANEL

LOGIN

User can login using Gmail or Facebook.
REGISTER

Basic account and contact information.
Once registered, a notification to be sent to the provided email.
PAYMENT

Payment could be processed through preferred payment gateway.
Booking details would be generated through SMS.
WALLET

Registered users would be capable of accessing wallet, where the day’s offer price would be transferred automatically to the wallet, at the time of booking.
Guest users can as well avail the offer, but the difference is that as they don’t have access to a wallet, the offer price would be directly deducted from the booked price.
PRINT  OR CANCEL

User can print/cancel his booking, and the refund would be processed either to the wallet or to his payment gateway account.
ROUTE MANAGEMENT

Select the source city
Add the destination city










Checkout products:


https://www.doditsolutions.com/book-my-show-clone/


http://scriptstore.in/product/book-myshow-clone-script/


http://phpreadymadescripts.com
